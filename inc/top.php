<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="jquery.fancybox.min.css">
    <link rel="stylesheet" href="css\style.css">
    <title>Kuvagalleria</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  
  <!--<ul class="navbar-nav">-->
    <div class="container">
      <div class="nav-item text-left">
        <p class="nav-link">Kuvagalleria</p>
      </div>

      <div class="nav-item text-right pull-right">
        <a class="nav-link" href="add.php">Lisää kuva<i class="fa fa-camera" aria-hidden="true"></i></a>
      </div>
  </div>  
    
  <!--</ul>-->
</nav>
    <div class"container">